# README

This is an adapter package for using QuickCheck tests with MuCheck. To use it,
checkout mucheck in another directory and do from the root of
mucheck-quickcheck

```
$ cabal sandbox init
$ cabal sandbox add-source ../mucheck
$ cabal install --dependencies-only
$ cabal configure
$ cabal build
```

You can also reuse the premade cabal sandbox from mucheck if you have already
installed it

```
$ cabal sandbox init --sandbox ../mucheck
$ cabal configure
$ cabal build
```
Using it.

```
$ cabal repl
> :l src/Main.hs
> :m + Test.MuCheck.TestAdapter
> :m + Test.MuCheck.TestAdapter.QuickCheck
> :m + Test.QuickCheck.Test
> mucheck (toRun "Examples/QuickCheckTest.hs" :: QuickCheckRun) [] >>= return . fst

If you have the HPC tix file, then

> mucheck (toRun "Examples/QuickCheckTest.hs" :: QuickCheckRun) "sample-test.tix" >>= return . fst

```
