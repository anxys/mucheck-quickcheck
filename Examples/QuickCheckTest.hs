module Examples.QuickCheckTest where
import Test.QuickCheck
import Data.List

qsort :: [Int] -> [Int]
qsort [] = []
qsort (x:xs) = qsort l ++ [x] ++ qsort r
    where l = filter (< x) xs
          r = filter (>= x) xs

uncoveredDummy :: Int -> Int
uncoveredDummy a = 0 + a

{-# ANN idEmpProp "Test" #-}
idEmpProp xs = qsort xs == qsort (qsort xs)

{-# ANN revProp "Test" #-}
revProp xs = qsort xs == qsort (reverse xs)

{-# ANN modelProp "Test" #-}
modelProp xs = qsort xs == sort xs

